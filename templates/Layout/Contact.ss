<div class="container mt-5">
    <div class="row">
        <div class="col-lg-4 col-md-6 col-xs-6 ls-m pt-3">
            <h2 class="font-weight-bold text-uppercase ls-m mb-2">Contact us</h2>
            <p>Fill in the form and get in touch</p>
            <ul>
                <% if $Address %>
                    <li class="mb-3">Address: $Address</li>
                <% end_if %>

                <% if $Tel %>
                    <li class="mb-3">Phone: <a href="tel:$Tel">$Tel</a></li>
                <% end_if %>

                <% if $Email %>
                    <li class="mb-3">Email: <a href="mailto:$Email">$Email</a></li>
                <% end_if %>

                <% if $OpeningHours %>
                    <li class="mb-3">Opening Hours: $OpeningHours</li>
                <% end_if %>
            </ul>
        </div>

        <div class="col-lg-7 contact-form pb-80">
            <form action="$AbsoluteLink" method="post" data-role="ajax-form">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 pl-0">
                            <div class="contact-form">
                                <h3 class="text-uppercase font-weight-bold py-3">Let's connect</h3>
                                <div class="form-group contuct_f">
                                    <label for="con_name">Name <span>*</span></label>
                                    <input type="text" class="form-control" id="con_name" name="name" placeholder="Name">
                                </div>
                                <div class="form-group contuct_f">
                                    <label for="con_email">Email <span>*</span></label>
                                    <input type="email" class="form-control" id="con_email" name="email" placeholder="Email">
                                </div>
                                <div class="form-group contuct_f">
                                    <label for="con_message">Your message <span>*</span></label>
                                    <textarea class="form-control" id="con_message" name="message" rows="3"></textarea>
                                </div>
                                <button type="submit" class="btn btn-default contact-btn">Submit</button>
                                <p class="form-messege"></p>
                                $IncludeReCaptchaField('contact')
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="page-content mt-10 mb-10 pb-5 pt-4">
        <% if $Theme.CustomField('MapLocation').Value %>
            <div class="pt-5 pb-4" id="map_frame_container" style="height:500px;">
                <input type="hidden" id="map_frame_query" value="$Theme.CustomField('MapLocation').Value" />
                <input type="hidden" id="map_frame_key" value="AIzaSyAWr-_Q_0Eg08q76abBeV3uhjj4jvYBif4" />
            </div>
        <% end_if %>
    </div>
</div>