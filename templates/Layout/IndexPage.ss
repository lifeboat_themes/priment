<% include SliderArea %>

<% include CollectionCard %>

<% if $Theme.CustomField('TabbedCarousel1').Value || $Theme.CustomField('TabbedCarousel2').Value() || $Theme.CustomField('TabbedCarousel3').Value() %>
    <% include CarouselCollections %>
<% end_if %>

<% include BannerArea Collection=$Theme.CustomField('BannerCollection').Value() %>

<% if $Theme.CustomField('DisplayCollection').Value() %>
    <% include DisplayCollection Collection=$Theme.CustomField('DisplayCollection').Value() %>
<% end_if %>

<% if $Blog.Children.count %>
    <section class="blog_section blog_black">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title">
                        <h2>Latest blog Posts</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="blog_wrapper blog_column3 owl-carousel">
                    <% loop $Blog.Children.sort('Created', 'Desc').limit(4) %>
                        <div class="col-lg-4">
                            <div class="single_blog">
                                <div class="blog_thumb">
                                    <% if $Image %>
                                        <a href="$AbsoluteLink"><img src="$Image.Fill(423,253).AbsoluteLink" alt="$Title"/></a>
                                    <% else %>
                                        <a href="$AbsoluteLink"><img src="$SiteSettings.Logo.Fill(423,253).AbsoluteLink" alt="$Title" /></a>
                                    <% end_if %>
                                </div>
                                <div class="blog_content">
                                    <h3><a href="$AbsoluteLink">$Title</a></h3>
                                    <div class="author_name">
                                        <p>$Created.Format('dd MMM YYYY')</p>
                                    </div>

                                    <div class="post_desc">
                                        <% if $MetaDescription %>
                                            <p>$MetaDescription.FirstParagraph.LimitWordCount(30)</p>
                                        <% else %>
                                            <p>$Content.FirstParagraph.LimitWordCount(30)</p>
                                        <% end_if %>
                                    </div>
                                    <div class="read_more">
                                        <a href="$AbsoluteLink">Continue reading</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <% end_loop %>
                </div>
            </div>
        </div>
    </section>
<% end_if %>

<% if $Integration('Mailchimp') %>
    <div class="newsletter_area newsletter_black">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="newsletter_content">
                        <h2>Subscribe to our newsletter</h2>
                        <div class="subscribe_form">
                            <form action="#" class="mc-form footer-newsletter mailchimp-subscribe" data-alert="#subscribe-alert">
                                <input type="email" name="email" id="email" placeholder="Enter your email address" required=""/>
                                <button type="submit">Subscribe</button>
                            </form>
                            <div class="mailchimp-alerts text-centre">
                                <div class="" id="subscribe-alert"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<% end_if %>