<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li>></li>
                        <li><a href="$Blog.AbsoluteLink">Blog</a></li>
                        <li>></li>
                        <li>$Title</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog_area blog_details">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog_details_wrapper">
                    <% if $Image %>
                        <div class="blog_thumb">
                            <img src="$Image.Fill(1290,500).Link" alt="$Title"/>
                        </div>
                    <% end_if %>
                    <div class="blog_content">
                        <h3 class="post_title">$Title</h3>
                        <div class="post_meta">
                            <span><i class="fa fa-calendar" aria-hidden="true"></i> $Created.format('MMM d Y') </span>
                        </div>
                        <div class="post_content">
                            $Content
                        </div>
                        <div class="entry_content">
                            <div class="social_sharing">
                                <ul>
                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=$AbsoluteLink" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/intent/tweet?text=$AbsoluteLink" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>