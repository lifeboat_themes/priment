<div class="container-fluid py-5">
    <main class="main account">
        <div class="page-content mt-10 mb-10">
            <div class="container margin-fix pt-1">
                <div class="tab tab-vertical">
                    <ul class="nav nav-tabs mb-4" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" href="#account">Account details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#address">Addresses</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#history">Order History</a>
                        </li>
                    </ul>
                    <% if $Loyalty.Enable %>
                        <div class="row mb-3">
                            <div class="col-12 text-right pr-0">
                                <div class="loyalty-points">
                                    <small>You currently have</small><br />
                                    <strong>$Customer.LoyaltyPoints</strong> $Loyalty.Name
                                </div>
                            </div>
                        </div>
                    <% end_if %>
                    <div class="tab-content">
                        <div class="tab-pane active" id="account">
                            $ProfileDetailsForm
                        </div>
                        <div class="tab-pane" id="address">
                            $ProfileAddressesForm
                        </div>
                        <div class="tab-pane" id="history">
                            $OrderHistory
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>